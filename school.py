import agency
import city


class School:
    def __init__(self, uid: str, name: str, agency_obj: agency.Agency, city_obj: city.City, mlocale: str, ulocale: str,
                 status: str):
        self.id = uid
        self.name = name
        self.agency = agency_obj
        self.city = city_obj
        self.mlocale = mlocale  # from initial looks it looks like ulocale and mlocale are the attributes of city but
        self.ulocale = ulocale  # as I was not sure I added it to the school. This would result in denormalised data
        self.status = status

    def search_terms(self) -> list:
        terms = []
        terms.extend(self.__get_search_term__(self.name))
        terms.extend(self.__get_search_term__(self.city.name))
        terms.extend(self.__get_search_term__(self.city.state.name))
        terms.extend(self.__get_search_term__(self.city.state.code))
        return terms

    def __get_search_term__(self, s: str):
        return list(set(s.lower().split(" ")))


SchoolDb = {}
SchoolSearchIndex = {}


def add_school(uid: str, name: str, agency_obj: agency.Agency, city_obj: city.City, mlocale: str, ulocale: str,
               status: str) -> School:
    school = School(uid, name, agency_obj, city_obj, mlocale, ulocale, status)
    SchoolDb[uid] = school
    words = school.search_terms()
    for word in words:
        if word in SchoolSearchIndex:
            l = SchoolSearchIndex[word]
            l.append(school.id)
        else:
            SchoolSearchIndex[word] = [school.id]
    return school


def search_school(search_term: str, limit: int = 10):
    search_term = search_term.lower()
    frequency = {}
    for word in search_term.split(" "):
        if word in SchoolSearchIndex:
            word_frequency = get_frequency(SchoolSearchIndex[word])
            frequency = collate_frequencies(frequency, word_frequency)
    if len(frequency) < 1:
        return None
    # sort by frequency
    search_result = sorted(frequency, key=frequency.get, reverse=True)
    if len(search_result) > limit:
        search_result = search_result[:limit]
    result = []
    for school_id in search_result:
        result.append(SchoolDb[school_id])
    return result


def get_frequency(l):
    frequency = {}
    for item in l:
        if item in frequency:
            frequency[item] += 1
        else:
            frequency[item] = 1
    return frequency


def collate_frequencies(master_frequency, word_frequency):
    for item in word_frequency:
        score = 1 + (word_frequency[item] * 0.1)
        # basically repetition of words is less ranked than having more matched words
        if item in master_frequency:
            master_frequency[item] += score

        else:
            master_frequency[item] = score
    return master_frequency


def count_total_schools() -> int:
    return len(SchoolDb)


def count_school_per(per):
    school_count = {}
    for school_id in SchoolDb:
        school = SchoolDb[school_id]
        per_key_value = ""
        if per == "city":
            per_key_value = school.city.name
        elif per == "state":
            per_key_value = school.city.state.code
        elif per == "mlocale":
            per_key_value = school.mlocale
        else:
            return None
        if per_key_value in school_count:
            school_count[per_key_value] += 1
        else:
            school_count[per_key_value] = 1
    return school_count
