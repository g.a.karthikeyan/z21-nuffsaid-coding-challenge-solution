# Z21 NuffSaid Coding Challenge Solution

## This is python3 project
### Please run this as
```
python3 main.py <csv_file>
```

**Result:**
```
z21-nuffsaid-coding-challenge-solution tm002$ python3 main.py school_data.csv
Loading csv and processing it.. Please wait..

Csv loaded..
Total Schools: 34779
Schools by State:
AL: 3260
AK: 652
AZ: 2697
AR: 1730
CA: 10707
OR: 1
NV: 1
CO: 1807
CT: 1020
DE: 104
FL: 3684
GA: 2070
HI: 287
ID: 653
IL: 3672
IN: 1336
OH: 1
IA: 1097
Schools by Metro-centric locale:
3: 8180
7: 4276
4: 3829
8: 4291
1: 4862
6: 2592
2: 5383
N: 1096
5: 270
City with most schools : CHICAGO (657 schools)
Unique cities with at least one school: 4852
Results for "AL" (search took: 0.00197s)
1. AL SCHOOL FOR DEAF
TALLADEGA, AL
2. AL SCHOOL FOR BLIND
TALLADEGA, AL
3. AL SCH OF MATH AND SCIENCE
MOBILE, AL
Results for "elementary school highland park" (search took: 0.02329s)
1. HIGHLAND PARK ELEMENTARY SCHOOL
MUSCLE SHOALS, AL
2. HIGHLAND PARK ELEMENTARY SCHOOL
PUEBLO, CO
3. HIGHLAND PARK HIGH SCHOOL
HIGHLAND PARK, CA
Results for "jefferson belleville" (search took: 0.00023s)
1. JEFFERSON ELEM SCHOOL
BELLEVILLE, AR
2. JEFFERSON HIGH SCHOOL
JEFFERSON, GA
3. JEFFERSON ELEMENTARY SCHOOL
JEFFERSON, GA
Results for "riverside school 44" (search took: 0.00955s)
1. RIVERSIDE SCHOOL 44
INDIANAPOLIS, IN
2. RIVERSIDE SCHOOL
RIVERSIDE, CA
3. RIVERSIDE ELEMENTARY SCHOOL
RIVERSIDE, CA
Results for "granada charter school" (search took: 0.01051s)
1. GRANADA HILLS CHARTER HIGH
GRANADA HILLS, CA
2. GRANADA ELEMENTARY SCHOOL
GRANADA, CO
3. GRANADA UNDIVIDED HIGH SCHOOL
GRANADA, CO
Results for "foley high alabama" (search took: 0.00652s)
1. FOLEY HIGH SCHOOL
FOLEY, AL
2. FOLEY MIDDLE SCHOOL
FOLEY, AL
3. FOLEY ELEMENTARY SCHOOL
FOLEY, AL
Results for "KUSKOKWIM" (search took: 1e-05s)
1. TOP OF THE KUSKOKWIM SCHOOL
NIKOLAI, AK
```
### If you are running this from a python3 command prompt, you the following commands

```
import util
util.load_data_from_csv("<csv_file_path>")

import count_schools
count_schools.print_counts()

import school_search
school_search.search_schools("AL")
school_search.search_schools("elementary school highland park")
school_search.search_schools("jefferson belleville")
school_search.search_schools("riverside school 44")
school_search.search_schools("granada charter school")
school_search.search_schools("foley high alabama")
school_search.search_schools("KUSKOKWIM")
```
**Result**
```
z21-nuffsaid-coding-challenge-solution tm002$ python3
Python 3.8.5 (default, Jul 21 2020, 10:42:08)
[Clang 11.0.0 (clang-1100.0.33.17)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> import util
>>> util.load_data_from_csv("school_data.csv")
>>>
>>> import count_schools
>>> count_schools.print_counts()
Total Schools: 34779
Schools by State:
AL: 3260
AK: 652
AZ: 2697
AR: 1730
CA: 10707
OR: 1
NV: 1
CO: 1807
CT: 1020
DE: 104
FL: 3684
GA: 2070
HI: 287
ID: 653
IL: 3672
IN: 1336
OH: 1
IA: 1097
Schools by Metro-centric locale:
3: 8180
7: 4276
4: 3829
8: 4291
1: 4862
6: 2592
2: 5383
N: 1096
5: 270
City with most schools : CHICAGO (657 schools)
Unique cities with at least one school: 4852
>>>
>>> import school_search
>>> school_search.search_schools("AL")
Results for "AL" (search took: 0.00181s)
1. AL SCHOOL FOR DEAF
TALLADEGA, AL
2. AL SCHOOL FOR BLIND
TALLADEGA, AL
3. AL SCH OF MATH AND SCIENCE
MOBILE, AL
>>> school_search.search_schools("elementary school highland park")
Results for "elementary school highland park" (search took: 0.023s)
1. HIGHLAND PARK ELEMENTARY SCHOOL
MUSCLE SHOALS, AL
2. HIGHLAND PARK ELEMENTARY SCHOOL
PUEBLO, CO
3. HIGHLAND PARK HIGH SCHOOL
HIGHLAND PARK, CA
>>> school_search.search_schools("jefferson belleville")
Results for "jefferson belleville" (search took: 0.00012s)
1. JEFFERSON ELEM SCHOOL
BELLEVILLE, AR
2. JEFFERSON HIGH SCHOOL
JEFFERSON, GA
3. JEFFERSON ELEMENTARY SCHOOL
JEFFERSON, GA
>>> school_search.search_schools("riverside school 44")
Results for "riverside school 44" (search took: 0.00976s)
1. RIVERSIDE SCHOOL 44
INDIANAPOLIS, IN
2. RIVERSIDE SCHOOL
RIVERSIDE, CA
3. RIVERSIDE ELEMENTARY SCHOOL
RIVERSIDE, CA
>>> school_search.search_schools("granada charter school")
Results for "granada charter school" (search took: 0.01222s)
1. GRANADA HILLS CHARTER HIGH
GRANADA HILLS, CA
2. GRANADA ELEMENTARY SCHOOL
GRANADA, CO
3. GRANADA UNDIVIDED HIGH SCHOOL
GRANADA, CO
>>> school_search.search_schools("foley high alabama")
Results for "foley high alabama" (search took: 0.00549s)
1. FOLEY HIGH SCHOOL
FOLEY, AL
2. FOLEY MIDDLE SCHOOL
FOLEY, AL
3. FOLEY ELEMENTARY SCHOOL
FOLEY, AL
>>> school_search.search_schools("KUSKOKWIM")
Results for "KUSKOKWIM" (search took: 2e-05s)
1. TOP OF THE KUSKOKWIM SCHOOL
NIKOLAI, AK
>>>
```