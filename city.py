from typing import Union
import state


class City:
    def __init__(self, name: str, state_code: str):
        self.name = name
        if state_code in state.StateDb:
            state_obj = state.StateDb[state_code]
        else:
            state_obj = state.State(state_code, state_code)
            state.StateDb[state_code] = state_obj
        self.state = state_obj


CityDb = {}


def get_or_create_city(name: str, state: str) -> City:
    if name in CityDb:
        return CityDb[name]
    else:
        city = City(name, state)
        CityDb[name] = city
        return city


def get_city(name: str) -> Union[City, None]:
    if name in CityDb:
        return CityDb[name]
    return None
