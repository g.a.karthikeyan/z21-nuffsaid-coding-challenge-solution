from util import load_data_from_csv
import count_schools
import school_search
import sys


def main(csv_path:str):
    print("Loading csv and processing it.. Please wait..\n")
    load_data_from_csv(csv_path)
    print("Csv loaded..")
    count_schools.print_counts()
    school_search.search_schools("AL")
    school_search.search_schools("elementary school highland park")
    school_search.search_schools("jefferson belleville")
    school_search.search_schools("riverside school 44")
    school_search.search_schools("granada charter school")
    school_search.search_schools("foley high alabama")
    school_search.search_schools("KUSKOKWIM")


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Program needs csv file that needs to be loaded")
        exit(1)
    main(sys.argv[1])
