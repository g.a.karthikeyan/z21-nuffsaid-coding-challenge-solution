from school import count_total_schools, count_school_per


def print_counts():
    school_count=count_total_schools()
    if school_count<1:
        print("Please load the data from csv using the util.load_data_from_csv function")
        return
    print("Total Schools:", school_count)

    print("Schools by State:")
    print_count_per(count_school_per("state"))

    print("Schools by Metro-centric locale:")
    print_count_per(count_school_per("mlocale"))

    count_per_city = count_school_per("city")
    if len(count_per_city)>0:
        # sorting the cities per descending order of count of schools in the city
        sorted_cities = sorted(count_per_city, key=count_per_city.get, reverse=True)

        print(f'City with most schools : {sorted_cities[0]} ({count_per_city[sorted_cities[0]]} schools)')

        # As the city list will only have the city with at least one city
        print("Unique cities with at least one school:", len(sorted_cities))
    else:
        print("No data loaded")


def print_count_per(count_per):
    for key in count_per:
        print(f'{key}: {count_per[key]}')
