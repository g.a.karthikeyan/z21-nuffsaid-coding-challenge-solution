from city import get_or_create_city
from school import add_school
from agency import get_or_create_agency
import csv


def load_data_from_csv(csv_path: str):
    with open(csv_path, encoding='cp1252') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            # note:  we are not loading the lat and lan info
            add_school_from_raw_data(row["NCESSCH"], row["LEAID"], row["LEANM05"], row["SCHNAM05"], row["LCITY05"],
                                     row["LSTATE05"], row["MLOCALE"], row["ULOCALE"], row["status05"])


def add_school_from_raw_data(school_id, agency_id, agency_name, school_name, city_name, state, mlocale, ulocale,
                             status):
    # city
    city = get_or_create_city(city_name, state)
    # agency
    agency = get_or_create_agency(agency_id, agency_name)
    add_school(school_id, school_name, agency, city, mlocale, ulocale, status)
