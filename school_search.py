from school import search_school
import time


def search_schools(search_term):
    start = time.time()
    schools = search_school(search_term, 3)
    end = time.time()
    elapsed_time = round(end - start, 5)
    print(f'Results for "{search_term}" (search took: {elapsed_time}s)')
    if schools is None:
        print("No result found")
        return
    i = 1
    for s in schools:
        print(f'{i}. {s.name}')
        print(f'{s.city.name}, {s.city.state.code}')
        i += 1
