from typing import Union


class Agency:
    def __init__(self, uid: str, name: str):
        self.id = uid
        self.name = name


AgencyDb = {}


def get_or_create_agency(uid: str, name: str) -> Agency:
    if uid in AgencyDb:
        return AgencyDb[uid]
    else:
        agency = Agency(uid, name)
        AgencyDb[uid] = agency
        return agency


def get_agency(uid: str) -> Union[Agency, None]:
    if uid in AgencyDb:
        return AgencyDb[uid]
    return None
